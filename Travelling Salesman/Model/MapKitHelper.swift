//
//  MapKitHelper.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 7/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import Foundation
import MapKit

let mapKitRequestQueue = DispatchQueue(label: "mapKitRequestQueue", qos: .default)

func requestRoutes(for destinations: [Destination], completionHandler: @escaping ([MKRoute?], [Error?]) -> Void)  {
    
    var numberOfRoutes = destinations.count - 1
    
    var routes : [MKRoute?] = Array(repeating: nil, count: numberOfRoutes)
    var errors : [Error?] = Array(repeating: nil, count: numberOfRoutes)
    
    for i in 0 ..< numberOfRoutes {
        let request: MKDirectionsRequest = MKDirectionsRequest()
        
        request.source = destinations[i].mapItem
        request.destination = destinations[i + 1].mapItem
        
        request.requestsAlternateRoutes = false
        
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        directions.calculate { (response, error) in
            mapKitRequestQueue.async {
                numberOfRoutes = numberOfRoutes - 1
                
                if let e = error {
                    logError("directions.calculate error: %@", e as CVarArg)
                    errors[i] = e
                }
                else if let route = response?.routes.first {
                    routes[i] = route
                }
                
                if numberOfRoutes == 0 {
                    completionHandler(routes, errors)
                }
            }
        }
    }
}

