//
//  Destination.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 4/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class Destination : NSObject, MKAnnotation {
    var route: MKRoute?
    
    dynamic var coordinate : CLLocationCoordinate2D = CLLocationCoordinate2D() {
        didSet {
            self.updateCoordinatesString()
        }
    }
    
    dynamic var title: String?
    dynamic var subtitle: String?
    
    private var _mapItem: MKMapItem?
    private var addressDictionary: [AnyHashable: Any]?
    
    var mapItem: MKMapItem {
        return _mapItem ?? MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary: self.addressDictionary as? [String: Any]))
    }
    
    override var debugDescription: String {
        return "\(title ?? subtitle ?? "") \(coordinate)"
    }
    
    var dictionary: [String: Any] {
        var dict: [String: Any] = [
            "latitude": coordinate.latitude,
            "longitude": coordinate.longitude
        ]
        
        if let t = title {
            dict["title"] = t
        }
        
        if let t = subtitle {
            dict["subtitle"] = t
        }
        
        dict["addressDictionary"] = mapItem.placemark.addressDictionary
        
        return dict
    }
    
    convenience init?(dictionary: [String: Any]) {
        guard
            let latitude = dictionary["latitude"] as? Double,
            let longitude = dictionary["longitude"] as? Double
            else {
                return nil
        }
        
        self.init(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude),
                  title: dictionary["title"] as? String,
                  subtitle: dictionary["subtitle"] as? String)
        
        addressDictionary = dictionary["addressDictionary"] as? [AnyHashable: Any]
    }
    
    init(mapItem: MKMapItem) {
        self._mapItem = mapItem
        self.coordinate = mapItem.placemark.coordinate
        self.title = mapItem.placemark.title
        self.subtitle = mapItem.placemark.subtitle
        super.init()
    }
    
    init(coordinate: CLLocationCoordinate2D, title: String? = nil, subtitle: String? = nil) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        super.init()
        
        if title == nil {
            updateCoordinatesString()
        }
    }
    
    func updateCoordinatesString() {
        self.title = "GPS Co-ordinates"
        self.subtitle = coordinate.string
    }
    
    func reverseGeo() {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)) { (placemarks, error) in
            guard let placemark =  placemarks?.first else {
                return
            }
            self.title = placemark.title
            self.subtitle = placemark.subtitle
            self.addressDictionary = placemark.addressDictionary
            
            NotificationCenter.default.post(name: .didUpdateDestination, object: self)
        }
    }
}

extension Destination {
    static func ==(lhs: Destination, rhs: Destination) -> Bool {
        return lhs.coordinate.latitude == rhs.coordinate.latitude && lhs.coordinate.longitude == rhs.coordinate.longitude
    }
    
    func distance(from other: Destination) -> CLLocationDistance {
        return self.coordinate.distance(from: other.coordinate)
    }
}

enum CodingKeys: String, CodingKey {
    case coordinate
    case title
    case subtitle
}

