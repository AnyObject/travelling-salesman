//
//  GeneticAlgorithm.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 7/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

/*
 **** Credits ****
 This program uses Open Source components. You can find the source code of their open source projects along with license information below. We acknowledge and are grateful to these developers for their contributions to open source.
 
 Project: DAPathfinder https://github.com/dagostini/DAPathfinder
 Copyright © 2018 Dejan. All rights reserved.
 */


import UIKit

class GeneticAlgorithm
{
    var populationSize = 500
    let mutationProbability = 0.015
    
    let destinations: [Destination]
    private var onNewGeneration: (Route, Int) -> Void
    
    private var population: [Route] = []

    init(destinations: [Destination], handler: @escaping (Route, Int) -> Void) {
        self.destinations = destinations
        self.onNewGeneration = handler
        self.population = self.randomPopulation(from: self.destinations)
    }
    
    private func randomPopulation(from destinations: [Destination]) -> [Route] {
        var result: [Route] = []
        for _ in 0..<populationSize {
            let randomDestinations = destinations.shuffle()
            result.append(Route(destinations: randomDestinations))
        }
        return result
    }
    
    private var evolving = false
    private var generation = 1
    
    public func startEvolution() {
        evolving = true
        DispatchQueue.global().async {
            while self.evolving {
                
                let currentTotalDistance = self.population.reduce(0.0, { $0 + $1.distance })
                let sortByFitnessDESC: (Route, Route) -> Bool = { $0.fitness(withTotalDistance: currentTotalDistance) > $1.fitness(withTotalDistance: currentTotalDistance) }
                let currentGeneration = self.population.sorted(by: sortByFitnessDESC)
                
                var nextGeneration: [Route] = []
                
                for _ in 0..<self.populationSize {
                    guard
                        let parentOne = self.getParent(fromGeneration: currentGeneration, withTotalDistance: currentTotalDistance),
                        let parentTwo = self.getParent(fromGeneration: currentGeneration, withTotalDistance: currentTotalDistance)
                        else { continue }
                    
                    let child = self.produceOffspring(firstParent: parentOne, secondParent: parentTwo)
                    let finalChild = self.mutate(child: child)
                    
                    nextGeneration.append(finalChild)
                }
                self.population = nextGeneration
                
                if let bestRoute = self.population.sorted(by: sortByFitnessDESC).first {
                    self.onNewGeneration(bestRoute, self.generation)
                }
                self.generation += 1
            }
        }
    }
    
    public func stopEvolution() {
        evolving = false
    }
    
    private func getParent(fromGeneration generation: [Route], withTotalDistance totalDistance: Double) -> Route? {
        let fitness = Double(arc4random()) / Double(UINT32_MAX)
        
        var currentFitness: Double = 0.0
        var result: Route?
        generation.forEach { (route) in
            if currentFitness <= fitness {
                currentFitness += route.fitness(withTotalDistance: totalDistance) //TODO: This is using the 'elitist' method, convert it to a 'roulette'
                result = route
            }
        }
        
        return result
    }
    
    private func produceOffspring(firstParent: Route, secondParent: Route) -> Route {
        let slice: Int = Int(arc4random_uniform(UInt32(firstParent.destinations.count)))
        var destinations: [Destination] = Array(firstParent.destinations[0..<slice])
        
        var idx = slice
        while destinations.count < secondParent.destinations.count {
            let destination = secondParent.destinations[idx]
            if destinations.contains(destination) == false {
                destinations.append(destination)
            }
            idx = (idx + 1) % secondParent.destinations.count
        }
        
        return Route(destinations: destinations)
    }
    
    private func mutate(child: Route) -> Route {
        if self.mutationProbability >= Double(Double(arc4random()) / Double(UINT32_MAX)) {
            let firstIdx = Int(arc4random_uniform(UInt32(child.destinations.count)))
            let secondIdx = Int(arc4random_uniform(UInt32(child.destinations.count)))
            var destinations = child.destinations
            destinations.swapAt(firstIdx, secondIdx)
            
            return Route(destinations: destinations)
        }
        
        return child
    }
}

extension Array {
    public func shuffle() -> [Element] {
        return sorted(by: { (_, _) -> Bool in
            return arc4random() < arc4random()
        })
    }
}
