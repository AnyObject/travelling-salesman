//
//  Route.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 7/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

/*
 **** Credits ****
 This program uses Open Source components. You can find the source code of their open source projects along with license information below. We acknowledge and are grateful to these developers for their contributions to open source.
 
 Project: DAPathfinder https://github.com/dagostini/DAPathfinder
 Copyright © 2018 Dejan. All rights reserved.
 */

import UIKit

class Route
{
    let destinations: [Destination]
    
    var _distance: Double?
    var distance: Double {
        if _distance == nil {
            _distance = calculateDistance()
        }
        return _distance ?? 0.0
    }
    
    init(destinations: [Destination]) {
        self.destinations = destinations
    }
    
    private func calculateDistance() -> Double {
        var result: Double = 0.0
        var previousDestination: Destination?
        
        destinations.forEach { (destination) in
            if let previous = previousDestination {
                result += previous.distance(from: destination)
            }
            previousDestination = destination
        }
        
        guard let first = destinations.first, let last = destinations.last else { return result }
        
        return result + first.distance(from: last)
    }
    
    // Probability of being selected from 0 to 1
    func fitness(withTotalDistance totalDistance: Double) -> Double {
        return 1 - (distance/totalDistance)
    }
}
