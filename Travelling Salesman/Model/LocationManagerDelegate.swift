//
//  LocationManagerDelegate.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 4/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import Foundation
import CoreLocation

//MARK: - CLLocationManagerDelegate
extension AppDelegate: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            logDebug("Location Service on")
        default:
            logDebug("Location Service off")
        }
    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        logError("didFailWithError: %@", error as CVarArg)
    }
    
    func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
        if let e = error {
            logError("didFinishDeferredUpdatesWithError: %@", e as CVarArg)
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        logDebug("didUpdateHeading %@", newHeading)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        logDebug("didUpdateLocations %@", locations)
        
        if let location = locations.sorted(by: {$0.timestamp > $1.timestamp }).last {
            self.lastLocation = location
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        logDebug("locationManagerDidPauseLocationUpdates")
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        logDebug("locationManagerDidResumeLocationUpdates")
    }
    
    
    func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
        logDebug("didVisit %@", visit)
    }
}
