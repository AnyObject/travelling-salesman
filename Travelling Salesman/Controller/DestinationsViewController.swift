//
//  DestinationViewController.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 4/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit
import MapKit

class DestinationsViewController: UIViewController {
    let headerHeight: CGFloat = 44
    let rowHeight: CGFloat = 44
    let routeRowHeight: CGFloat = 62
    let summaryRowHeight: CGFloat = 78
    
    var totalDistance: CLLocationDistance = 0
    var totalTime: TimeInterval = 0
    
    var toggleView = true
    
    var enableToggleView = false
    
    weak var mapViewController: ViewController!
    
    var geneticAlgorithm: GeneticAlgorithm?
    
    var desiredHeight: CGFloat {
        if !enableToggleView {
            return CGFloat.greatestFiniteMagnitude
        }
        
        var height = headerHeight
        
        if !toggleView {
            return height
        }
        
        if totalDistance > 0 {
            height += summaryRowHeight
        }
        
        for destination in app.destinations {
            height += rowHeight
            if destination.route != nil {
                height += routeRowHeight
            }
        }
        
        return height
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var directionsButton: RoundedUIButtonWithActivityIndicator!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBAction func directionsButtonTapped(_ sender: Any) {
        guard
            let button = sender as? RoundedUIButtonWithActivityIndicator,
            let action = button.title(for: .normal),
            let map = mapViewController.mapView else {
            return
        }
        
        switch action {
        case "DIRECTIONS":
            guard let origin = app.destinations.first
                else {
                    self.showAlert(message: "Please add a location to continue.")
                    return
            }
            
            titleLabel.text = "Calculating..."
            progressBar.progress = 0
            button.setImage(nil, for: .normal) 
            button.setTitle("STOP", for: .normal)
            button.enabledBackgroundColor = .red
            
            mapViewController.hideSearchBar(animated: true)
            
            let generations = min(app.destinations.count * 8, 300)
            
            self.geneticAlgorithm = GeneticAlgorithm(destinations: app.destinations, handler: { (route, generation) in
                
                var solution = route.destinations
                
                if let i = solution.index(of: origin) {
                    solution = Array(solution.suffix(from: i) + solution.prefix(upTo: i))
                }
                
                DispatchQueue.main.async {
                    self.progressBar.progress = Float(generation) / Float(generations)
                    
                    self.tableView.beginUpdates()
                    
                    for i in 0 ..< app.destinations.count {
                        if let j = solution.index(of: app.destinations[i]) {
                            let old = IndexPath(row: i * 2, section: 0)
                            let new = IndexPath(row: j * 2, section: 0)
                            
                            self.tableView.moveRow(at: old, to: new)
                            
                            if let cell = self.tableView.cellForRow(at: new) as?  DestinationCell {
                                cell.numberView.text = "\(j + 1)"
                            }
                        }
                    }
                    
                    app.destinations = solution
                    
                    self.tableView.endUpdates()
                    
                    if generation >= generations {
                        self.geneticAlgorithm?.stopEvolution()
                        self.geneticAlgorithm = nil
                        
                        self.fetchRoutes()
                    }
                }
            })
            
            self.geneticAlgorithm?.startEvolution()
        case "EDIT":
            map.removeOverlays(map.overlays)
            map.deselectAnnotation(map.selectedAnnotations.first, animated: true)
            
            for destination in app.destinations {
                destination.route = nil
            }
            
            totalDistance = 0
            totalTime = 0
            
            self.resetView()
            
            self.tableView.reloadSections([0], with: .automatic)
            
            mapViewController.showSearchBar(animated: true)
        case "STOP":
            self.geneticAlgorithm?.stopEvolution()
            
            self.showAlert(message: "Do you want to use the current results?", title: "Calculation was interrupted" , with: [
                UIAlertAction(title: "Yes", style: .destructive, handler: { (action) in
                    self.fetchRoutes()
                }),
                UIAlertAction(title: "No", style: .default, handler: { (action) in
                    self.progressBar.progress = 0
                    self.resetView()
                })
                ]
            )
            
        default:
            return
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName:  "DestinationCell", bundle: nil), forCellReuseIdentifier: "destination")
        self.tableView.register(UINib(nibName:  "RouteCell", bundle: nil), forCellReuseIdentifier: "route")
        self.tableView.register(UINib(nibName:  "SummaryCell", bundle: nil), forCellReuseIdentifier: "summary")
        
        self.progressBar.progressTintColor = .directions
        self.progressBar.alpha = 0.1
        
        NotificationCenter.default.addObserver(forName: .didSetDestinations, object: nil, queue: .main) { (_) in
            self.directionsButton.isEnabled = app.destinations.count > 1
        }
        
        NotificationCenter.default.addObserver(forName: .didUpdateDestination, object: nil, queue: .main) { (notification) in
            if let destination = notification.object as? Destination, let i = app.destinations.index(of: destination) {
                self.tableView.reloadRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
            }
        }
        
        resetView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension DestinationsViewController {
    
    private func resetView() {
        directionsButton.isUserInteractionEnabled = true
        
        self.directionsButton.isEnabled = app.destinations.count > 1

        if totalDistance > 0 {
            directionsButton.setTitle("EDIT", for: .normal)
            directionsButton.setImage(nil, for: .normal)
            directionsButton.enabledBackgroundColor = .red
            titleLabel.text = "Directions"
            progressBar.progress = 1
        } else {
            directionsButton.setTitle("DIRECTIONS", for: .normal)
            directionsButton.setImage(#imageLiteral(resourceName: "route-25"), for: .normal)
            directionsButton.enabledBackgroundColor = .directions
            titleLabel.text = "Destinations"
            progressBar.progress = 0
        }
    }
    
    private func fetchRoutes()  {
        let destinations = app.destinations
        
        guard let origin = destinations.first, let map = mapViewController.mapView else {
            return
        }
        
        self.directionsButton.isUserInteractionEnabled = false
        self.directionsButton.showSpinner()
        self.titleLabel.text = "Getting routes..."
        
        requestRoutes(for: destinations + [origin]) { (mkRoutes, errors) in
            
            logDebug("routes: %@", mkRoutes)
            
            DispatchQueue.main.async {
                
                for i in 0 ..< mkRoutes.count {
                    let mkRoute = mkRoutes[i]
                    app.destinations[i].route = mkRoute
                    if let r = mkRoute {
                        self.totalDistance += r.distance
                        self.totalTime += r.expectedTravelTime
                    }
                    else {
                        if i < destinations.count - 1 {
                            self.totalDistance += destinations[i].distance(from: destinations[i + 1])
                        }
                        self.totalTime = 0
                    }
                }
                
                if let e = errors.flatMap({$0 as NSError?}).first {
                    self.mapViewController.showSearchBar(animated: true)
                    self.showAlert(message: e.localizedFailureReason ?? e.localizedDescription, title: "Some routes may not be available")
                    
                    self.totalDistance = 0
                }
                else {
                    map.addOverlays(mkRoutes.flatMap({$0?.polyline}))
                }
                
                
                self.directionsButton.hideSpinner()
                self.resetView()
                
                self.tableView.reloadSections([0], with: .automatic)
            }
        }
    }
}

extension DestinationsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if app.destinations.isEmpty {
            self.tableView.allowsSelection = false
            return 1
        }
        
        self.tableView.allowsSelection = true
        
        var rows = app.destinations.count * 2
        
        if totalDistance > 0 {
            rows += 1
        }
        
        return rows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell 
    {
        if app.destinations.isEmpty {
            return tableView.dequeueReusableCell(withIdentifier: "empty", for: indexPath)
        }
        
        let i = Int(indexPath.row / 2)
        
        if indexPath.row >= app.destinations.count * 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "summary", for: indexPath) as! SummaryCell
            
            guard totalDistance > 0, let origin = app.destinations.first else {
                cell.captionLabel.text = nil
                cell.detailsLabel.text = nil
                return cell
            }
            
            cell.captionLabel.text = "Back to \(origin.title ?? "origin")"
            
            var details: [String] = []
            
            if let dist = distanceString(totalDistance) {
                details.append("Total distance: \(dist)")
            }
            
            if totalTime > 0 {
                details.append("Total est. time: \(durationString(totalTime))")
            }
            else {
                details.append("Total est. time: not avavilable")
            }
            
            cell.detailsLabel.text = details.joined(separator: "\n")
            
            return cell
        }
        else if indexPath.row % 2 == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "destination", for: indexPath) as! DestinationCell
            
            cell.numberView.text = "\(i + 1)"
            cell.titleLabel.text = app.destinations[i].title
            cell.subTitleLabel.text = app.destinations[i].subtitle
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "route", for: indexPath) as! RouteCell
            
            
            if let route = app.destinations[i].route {
                cell.contentView.alpha = 1
                
                var details: [String] = []
                
                if let dist = distanceString(route.distance) {
                    details.append("Distance: \(dist)")
                }
                
                details.append("Est. time: \(durationString(route.expectedTravelTime))")
                                
                cell.detailsLabel.text = details.joined(separator: "\n")
                
                cell.accessoryType = .detailDisclosureButton
            }
            else {
                cell.accessoryType = .none
                cell.contentView.alpha = 0
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let i = Int(indexPath.row / 2)
        
        if indexPath.row >= app.destinations.count * 2 {
            return summaryRowHeight
        }
        else if indexPath.row % 2 == 0 {
            return rowHeight
        }
        else {
            return  app.destinations[i].route != nil ? routeRowHeight : 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let i = Int(indexPath.row / 2)
        
        if i >= app.destinations.count {
            return
        }
        
        let destination = app.destinations[i]
        let map = mapViewController.mapView!
        
        map.overlays.flatMap({map.renderer(for: $0) as? MKPolylineRenderer}).forEach { (renderer) in
            if renderer.strokeColor != .defaultStroke {
                renderer.strokeColor = .defaultStroke
                renderer.lineWidth = defaultLineWidth
                renderer.setNeedsDisplay()
            }
        }
        
        if indexPath.row % 2 == 0 {
            map.selectAnnotation(destination, animated: true)
        }
        else {
            map.deselectAnnotation(map.selectedAnnotations.first, animated: true)

            if let route = destination.route, let renderer = map.renderer(for: route.polyline) as? MKPolylineRenderer {
                
                renderer.strokeColor = .selectedStroke
                renderer.lineWidth = selectedLineWidth
                renderer.setNeedsDisplay()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
        guard app.destinations.count > 1 else {
            return
        }
        
        let i = Int(indexPath.row / 2)
        
        var destinations = [app.destinations[i].mapItem]
        
        if i < app.destinations.count - 1  {
            destinations.append(app.destinations[i + 1].mapItem)
        }
        else {
            destinations.append(app.destinations[0].mapItem)
        }
        
        MKMapItem.openMaps(with: destinations, launchOptions: [
            MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving
            
            ]
        )
    }
    
    private func deleteRow(_ tableView: UITableView,  indexPath: IndexPath ) {
        
        let i = Int(indexPath.row / 2)
        let removed = app.destinations.remove(at: i)
        let map = self.mapViewController.mapView!
        
        map.removeAnnotation(removed)
        map.showAnnotations(map.annotations, animated: true)
        
        if app.destinations.isEmpty {
            tableView.reloadSections([0], with: .automatic)
        }
        else {
            tableView.deleteRows(at: [indexPath, IndexPath(row: i * 2 + 1, section: 0) ], with: .automatic)
        }
        
        tableView.reloadSections([0], with: .automatic)
    }
    
    private func canDeleteRow(at indexPath: IndexPath) -> Bool {
        if totalDistance == 0 {
            return !app.destinations.isEmpty
        }
        else {
            return false
        }
    }
    
    @available(iOS 11.0, *)
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if !canDeleteRow(at: indexPath) {
            return UISwipeActionsConfiguration(actions: [])
        }
        
        let delete = UIContextualAction(style: .destructive, title: "delete") { (action, view, handler) in
            self.deleteRow(tableView, indexPath: indexPath)
            
            handler(true)
        }
        
        delete.image = #imageLiteral(resourceName: "trash-25")
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    @available(iOS, deprecated:11.0, message: "Use trailingSwipeActionsConfigurationForRowAt()")
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if !canDeleteRow(at: indexPath) {
            return nil
        }
        
        let delete = UITableViewRowAction(style: .destructive, title: "-") { (action, index) in
            self.deleteRow(tableView, indexPath: indexPath)
        }
        
        return [delete]
    }
    
    @available(iOS, deprecated:11.0, message: "Use trailingSwipeActionsConfigurationForRowAt()")
    func tableView(_ tableView: UITableView, canPerformAction action: Selector, forRowAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return canDeleteRow(at: indexPath)
    }
}



