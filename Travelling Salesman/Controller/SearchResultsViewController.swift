//
//  SearchResultsViewController.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 5/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit
import MapKit
import UserNotifications

class SearchResultsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private let rowHeight: CGFloat = 44
    
    weak var mapViewController: ViewController!
    
    var error: NSError?
    
    var collection: [MKMapItem] {
        return mapViewController.searchResults
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.allowsSelection = false
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        
        self.tableView.register(UINib(nibName:  "DestinationCell", bundle: nil), forCellReuseIdentifier: "destination")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func addButtonTapped(gestureRecognizer: UIGestureRecognizer) {
                
        if let row = gestureRecognizer.view?.tag {
            let destination = Destination(mapItem: collection[row])
            
            //add to model
            app.destinations.append(destination)
            let now = Date()
            app.scheduleNotification("\(now.timeIntervalSince1970)", message: kNotificationMessage, at: now.addingTimeInterval(5 * 60))
            
            //add to map
            mapViewController.mapView.addAnnotation(destination)
        }
        
        self.performSegue(withIdentifier: "doneSearch", sender: self)
    }
}

extension SearchResultsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if error != nil {
            return 1
        }
        
        return collection.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if error != nil {
            return tableView.dequeueReusableCell(withIdentifier: "error", for: indexPath)
        }
        
        let row = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "destination", for: indexPath) as! DestinationCell
        
        cell.numberView.text = "+"
        cell.numberView.tag = row
        cell.numberView.backgroundColor = .add
        cell.titleLabel.text = collection[row].placemark.title
        cell.subTitleLabel.text = collection[row].placemark.subtitle
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(addButtonTapped))
        cell.numberView.addGestureRecognizer(tapRecognizer)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }

}
