//
//  ViewController.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 3/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit
import MapKit


let defaultLineWidth: CGFloat = 4
let selectedLineWidth: CGFloat = 6

extension UIColor {
    static let directions = UIColor(red: 63/255, green: 80/255, blue: 142/255, alpha: 1)
    static let add = UIColor(red: 45/255, green: 163/255, blue: 176/255, alpha: 1)
    static let defaultStroke = UIColor.blue.withAlphaComponent(0.3)
    static let selectedStroke = UIColor.blue.withAlphaComponent(0.9)
}


extension UIViewController {
    func showAlert(message: String, title: String? = nil, with actions: [UIAlertAction] =
        [
            UIAlertAction(title: "OK", style: .default, handler: { (action) in
            })
        ])
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alert.addAction(action)
        }
        
        if #available(iOS 10.0, *) {
            let impactGenerator = UIImpactFeedbackGenerator(style: .medium)
            impactGenerator.impactOccurred()
        }
        
        self.present(alert, animated: true) {
        }
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var destinationsView: UIView!
    @IBOutlet weak var destinationsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var currentLocationButton: RoundedUIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchResultsView: UIView!
    @IBOutlet weak var searchResultsViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var confirmViewHeightConstraint: NSLayoutConstraint!
    
    weak var destinationsViewController: DestinationsViewController!
    
    weak var searchResultsViewController: SearchResultsViewController!
    
    var userLocation: Destination?
    
    var searchResults: [MKMapItem] = []
    
    @IBAction func unwindToHome(_ sender: UIStoryboardSegue) {
        
        if sender.identifier == "doneSearch" {
            doneSearch()
        }
        else {
            if let destination = userLocation {
                if sender.identifier == "confirm" {
                    destination.reverseGeo()
                    app.destinations.append(destination)
                    
                    let now = Date()
                    app.scheduleNotification("\(now.timeIntervalSince1970)", message: kNotificationMessage, at: now.addingTimeInterval(5 * 60))
                }
                else {
                    mapView.removeAnnotation(destination)
                }
            }
            
            doneLocation()
        }
    }
    
    @IBAction func currentLocationButtonTapped(_ sender: Any) {
        
        let coordinate: CLLocationCoordinate2D!
        
        coordinate = mapView.userLocation.coordinate
        
        let annotation = Destination(coordinate: coordinate)
        self.userLocation = annotation
        
        hideSearchBar(animated: true)
        
        hideDestinations(animated: true, completion: {
                self.showConfirmButtons(animated: true)
            }
        )
        
        self.mapView.addAnnotation(annotation)
        self.mapView.showAnnotations([annotation], animated: true)
        self.mapView.selectAnnotation(annotation, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.showsPointsOfInterest = true
        
        mapView.delegate = self
        searchBar.delegate = self
        
        self.searchResultsView.alpha = 0
        self.confirmView.alpha = 0
        
        hideConfirmButtons(animated: false)
        
        hideSearchResults(animated: false)
        
        showDestinations(animated: false)
        
        NotificationCenter.default.addObserver(forName: .willPresentNotification, object: nil, queue: .main) { (notification) in
            if let message = notification.object as? String {
                self.showAlert(message: message)
            }
        }
        
        
        self.mapView.showsUserLocation = true
        self.mapView.addAnnotations(app.destinations)
        self.mapView.showAnnotations(app.destinations, animated: true)
        
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var keyCommands: [UIKeyCommand]? {
        return [
            UIKeyCommand(input: "\u{8}", modifierFlags: [], action: #selector(backKeyPressed))
        ]
    }
  
    @objc func backKeyPressed() {
        if let t = self.searchBar.text, t.count > 0 {
            let newText = String(t.prefix(t.count - 1))
            self.searchBar.text = newText
            self.searchBar(self.searchBar, textDidChange: newText)
        }
        else {
            doneSearch()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? DestinationsViewController {
            self.destinationsViewController = vc
            vc.mapViewController = self
        } else if let vc = segue.destination as? SearchResultsViewController {
            self.searchResultsViewController = vc
            vc.mapViewController = self
        }
    }
}


extension ViewController {
   
    func doneSearch() {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        searchBar.backgroundColor = nil
        searchBar.isTranslucent = true
        
        self.destinationsViewController.toggleView = true
        self.destinationsViewController.tableView.reloadData()
        
        hideSearchResults(animated: true, completion: {
            self.searchResults = []
            self.searchResultsViewController.tableView.reloadData()
            self.showDestinations(animated: true)
        })
    }
    
    func doneLocation(){
        userLocation = nil
        
        self.destinationsViewController.toggleView = true
        self.destinationsViewController.tableView.reloadData()
        
        hideConfirmButtons(animated: true, completion: {
            self.showSearchBar(animated: true)
            self.showDestinations(animated: true)
        })
    }
    
    func hideSearchBar(animated: Bool){
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.searchBar.alpha = 0
                self.currentLocationButton.alpha = 0
        })
    }
    
    func showSearchBar(animated: Bool){
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.searchBar.alpha = 1
                self.currentLocationButton.alpha = 1

        })
    }

    func hideDestinations(animated: Bool, completion: @escaping () -> Void = {}) {
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.destinationsViewHeightConstraint.constant = self.destinationsViewController.rowHeight * 2
                self.view.layoutIfNeeded()
                self.destinationsView.layoutIfNeeded()
        }, completion: { _ in
            self.destinationsView.layer.removeAllAnimations()
            completion()
        })
    }
    
    func showDestinations(animated: Bool, completion: @escaping () -> Void = {}) {
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.destinationsViewHeightConstraint.constant = min (
                    self.view.bounds.height / 2,
                    self.destinationsViewController.desiredHeight
                )
                self.view.layoutIfNeeded()
                self.destinationsView.layoutIfNeeded()
                
        }, completion: { _ in
            self.destinationsView.layer.removeAllAnimations()
            
            if !app.destinations.isEmpty {
                self.mapView.showAnnotations(self.mapView.annotations, animated: true)
            }
            
            completion()
        })
    }
    
    func showConfirmButtons(animated: Bool, completion: @escaping () -> Void = {}) {
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.confirmView.alpha = 1
                self.confirmViewHeightConstraint.constant = self.destinationsViewController.rowHeight * 2
                self.view.layoutIfNeeded()
                self.confirmView.layoutIfNeeded()
        }, completion: { _ in
            self.confirmView.layer.removeAllAnimations()
            
            completion()
        })
    }
    
    func hideConfirmButtons(animated: Bool, completion: @escaping () -> Void = {}) {
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.confirmViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
                self.confirmView.layoutIfNeeded()
                self.confirmView.alpha = 0
        }, completion: { _ in
            self.confirmView.layer.removeAllAnimations()
            
            completion()
        })
    }
    
    func showSearchResults(animated: Bool, completion: @escaping () -> Void = {}) {
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.searchResultsView.alpha = 1
                self.searchResultsViewHeightConstraint.constant = self.view.bounds.height - 85
                self.view.layoutIfNeeded()
                self.searchResultsView.layoutIfNeeded()
        }, completion: { _ in
            self.searchResultsView.layer.removeAllAnimations()
            
            completion()
        })
    }
    
    func hideSearchResults(animated: Bool, completion: @escaping () -> Void = {}) {
        UIView.animate(
            withDuration: animated ? 0.25 : 0,
            animations: {
                self.searchResultsViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
                self.searchResultsView.layoutIfNeeded()
                self.searchResultsView.alpha = 0
        }, completion: { _ in
            self.searchResultsView.layer.removeAllAnimations()
            
            completion()
        })
    }
}

extension ViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        if let annotion = userLocation {
            annotion.coordinate = mapView.centerCoordinate
            mapView.selectAnnotation(annotion, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        if !app.destinations.isEmpty || self.userLocation != nil {
            return
        }
        
        mapView.setRegion(MKCoordinateRegion(center: userLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)), animated: true)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if (overlay is MKPolyline) {
            polylineRenderer.strokeColor = .defaultStroke
            polylineRenderer.lineWidth = defaultLineWidth
        }
        return polylineRenderer
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let a = view.annotation as? Destination, let i = app.destinations.index(of: a) {
            self.destinationsViewController.tableView.selectRow(at: IndexPath(row: i * 2, section: 0), animated: true, scrollPosition: .top)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if mapView.selectedAnnotations.isEmpty {
            if let table = destinationsViewController.tableView, let selected = table.indexPathForSelectedRow, selected.row % 2 == 0 {
                table.deselectRow(at: selected, animated: true)
            }
        }
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.backgroundColor = .white
        searchBar.isTranslucent = true

        self.showSearchResults(animated: true)
    }
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        logDebug("textDidChange", searchBar)
        
        if searchText.isEmpty {
            self.searchResults = []
            self.searchResultsViewController.tableView.reloadSections([0], with: .automatic)
            return
        }
            
        let request = MKLocalSearchRequest()
        
        request.naturalLanguageQuery = searchText
        request.region = mapView.region
        
        let search = MKLocalSearch(request: request)
        
        search.start(completionHandler: { (response, error) in
            guard let response = response else {
                if let e = error as NSError? {
                    logError("MKLocalSearch error: %@", e as CVarArg)
                    self.searchResultsViewController.error = e
                    self.searchResultsViewController.tableView.reloadSections([0], with: .automatic)
                }
                
                return
            }
            
            DispatchQueue.main.async {
                self.searchResultsViewController.error = nil
                self.searchResults = response.mapItems
                self.searchResultsViewController.tableView.reloadSections([0], with: .automatic)
            }
        })
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        doneSearch()
    }
}
