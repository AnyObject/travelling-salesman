//
//  Helper.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 4/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import Foundation
import os.log
import CoreLocation

//MARK: - STRING

let kNotificationMessage = "You have added a destination recently."

var distanceFormatter: NumberFormatter {
    let formatter = NumberFormatter()
    formatter.maximumSignificantDigits = 2
    formatter.positiveSuffix = NSLocalizedString(" m", comment: "")
    return formatter
}

var distanceFormatterInKm: NumberFormatter {
    let formatter = NumberFormatter()
    formatter.maximumSignificantDigits = 4
    formatter.usesGroupingSeparator = true
    formatter.positiveSuffix = NSLocalizedString(" km", comment: "")
    return formatter
}

let dateFormatter: DateFormatter = {
    var formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    return formatter
}()

let timeFormatter: DateFormatter = {
    var formatter = DateFormatter()
    formatter.dateFormat = "HH:mm:ss"
    
    return formatter
}()

let shortTimeFormatter: DateFormatter = {
    var formatter = DateFormatter()
    formatter.dateFormat = "HH:mm"
    
    return formatter
}()

func distanceString(_ distance: Double) -> String? {
    var string: String?
    if distance >= 1000 {
        string = distanceFormatterInKm.string(from: (distance / 1000) as NSNumber)
    }
    else {
        string = distanceFormatter.string(from: distance as NSNumber)
    }
    
    return string
}


func durationString(_ t: TimeInterval) -> String {
    
    let minutes = Int(t / 60) % 60
    let hours = Int(t / 3600) % 24
    let days = Int(t / 3600 / 24)
    
    if days > 0 {
        return String(format: "%d day\(days > 1 ? "s" : "") %d hour\(hours > 1 ? "s" : "") %d min", days, hours, minutes)
    }
    
    if hours > 0 {
        return String(format: "%d hour\(hours > 1 ? "s" : "") %d min", hours, minutes)
    }
   
    if minutes > 0 {
        return String(format: "%d min", minutes)
    }
    
    return "< 1 min"
}

//MARK: - LOGGING

func logInfo(_ message: StaticString, _ args: CVarArg...) {
    if #available(iOS 10.0, *) {
        os_log(message, type: .info, args)
    } else {
        debugPrint(message, args)
    }
}


func logDebug(_ message: StaticString, _ args: CVarArg...) {
    if #available(iOS 10.0, *) {
        os_log(message, type: .debug, args)
    } else {
        debugPrint(message, args)
    }
}


func logError( _ message: StaticString, _ args: CVarArg...) {
    if #available(iOS 10.0, *) {
        os_log(message, type: .error, args)
    } else {
        debugPrint(message, args)
    }
}

//MARK: - LOCATION

extension CLLocationCoordinate2D {
    var string: String {
        let lat = self.latitude
        let long = self.longitude
        
        let ns = lat > 0 ? "N" : "S"
        let ew = long > 0 ? "E" : "W"
        return  "\(String(format: "%0.6f˚", fabs(lat)))\(ns), \(String(format: "%0.6f˚", fabs(long)))\(ew)"
    }

    func distance(from coordinate: CLLocationCoordinate2D) -> CLLocationDistance {
        return CLLocation(latitude: self.latitude, longitude: self.longitude).distance(from: CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude))
    }
}

extension CLPlacemark {
    var longAddress: String {
        var strings: [String] = []
        
        // Location name
        if let locationName = addressDictionary!["Name"] as? String {
            strings.append(locationName)
        }
        
        // Street address
        if let street = addressDictionary!["Thoroughfare"] as? String {
            if !strings.contains(where: {$0.contains(street)}) {
                strings.append(street)
            }
        }
        
        // City
        if let city = addressDictionary!["City"] as? String {
            strings.append(city)
        }
        
        if let loc = addressDictionary!["Sublocality"] as? String {
            if !strings.contains(where: {$0.contains(loc)}) {
                strings.append(loc)
            }
        }
        
        if let state = addressDictionary!["State"] as? String {
            if !strings.contains(where: {$0.contains(state)}) {
                strings.append(state)
            }
        }
        
        // Zip code
        //        if let zip = addressDictionary!["ZIP"] as? String {
        //            strings.append(zip)
        //        }
        
        // Country
        if let country = addressDictionary!["Country"] as? String {
            strings.append(country)
        }
        
        return strings.joined(separator: NSLocalizedString(", ", comment: ""))
    }
    
    var shortAddress: String {
        var strings: [String] = []
        
        // Street address
        if let street = addressDictionary!["Thoroughfare"] as? String {
            strings.append(street)
        }
        
        // City
        if let city = addressDictionary!["City"] as? String {
            strings.append(city)
        }
        
        return strings.joined(separator: NSLocalizedString(", ", comment: ""))
    }
    
    var streetAddress: String {
        var strings: [String] = []
        
        // Street address
        if let street = addressDictionary!["Thoroughfare"] as? String {
            strings.append(street)
        }
        
        return strings.joined(separator: NSLocalizedString(", ", comment: ""))
    }
    
    var cityAddress: String {
        var strings: [String] = []
        
        // City
        if let city = addressDictionary!["City"] as? String {
            strings.append(city)
        }
        
        if let loc = addressDictionary!["Sublocality"] as? String {
            if !strings.contains(where: {$0.contains(loc)}) {
                strings.append(loc)
            }
        }
        
        return strings.joined(separator: NSLocalizedString(", ", comment: ""))
    }
    
    var title: String? {
        var names: [String] = []
        
        if let name = name {
            names.append(name)
        }
        
        if let street = thoroughfare {
            var streetNumber = ""
            
            if let number = subThoroughfare {
                streetNumber = "\(number) \(street)"
            }
            else {
                streetNumber = street
            }
            
            if name != streetNumber {
                names.append(streetNumber)
            }
        }
        
        return names.joined(separator: ", ")
    }
    
    var subtitle: String? {
        var names: [String] = []
        
        if let local = locality ?? subLocality {
            names.append(local)
        }
        
        if let state = administrativeArea ?? subAdministrativeArea ?? country ?? inlandWater ?? ocean {
            names.append(state)
            
        }
        
        return names.joined(separator: ", ")
    }
}


