//
//  NotificationName.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 8/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import Foundation

extension Notification.Name {
    static var didSetDestinations: Notification.Name { return Notification.Name("didSetDestinations") }
    static var willPresentNotification: Notification.Name { return Notification.Name("willPresentNotification") }
    static var didUpdateDestination: Notification.Name { return Notification.Name("didUpdateDestination") }
}
