//
//  DestinationsTableViewCell.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 5/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit

class DestinationCell: UITableViewCell {
    
    @IBOutlet weak var numberView: NumberView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
