//
//  RouteCell.swift
//  Travelling Salesman
//
//  Created by User on 10/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit

class RouteCell: UITableViewCell {

    @IBOutlet weak var routeImageView: UIImageView!
    @IBOutlet weak var detailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        routeImageView.image = #imageLiteral(resourceName: "dots-50").withRenderingMode(.alwaysTemplate)
        routeImageView.tintColor = .directions
//        routeImageView.alpha = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
