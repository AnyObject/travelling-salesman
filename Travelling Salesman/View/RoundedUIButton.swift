//
//  RoundedUIButton.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 5/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedUIButton: UIButton {
    
    private var ratio: CGFloat = 0.5
    
    @IBInspectable
    var enabledBackgroundColor: UIColor? {
        didSet {
            if self.isEnabled {
                self.backgroundColor = enabledBackgroundColor
            }
        }
    }
    
    @IBInspectable
    var disabledBackgroundColor: UIColor? {
        didSet {
            if !self.isEnabled {
                self.backgroundColor = disabledBackgroundColor
            }
        }
    }
    
    @IBInspectable
    var radiusRatioToHeight : CGFloat {
        get { return ratio}
        set { ratio = min (0.5, max(0, newValue))}
    }
    
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        self.layer.cornerRadius = rect.height * ratio
        self.clipsToBounds = true
        
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.numberOfLines = 0
    }
    
    override open var isEnabled: Bool {
        didSet {
            if isEnabled, let c = self.enabledBackgroundColor {
                self.backgroundColor = c
            }
            
            if !isEnabled, let c = self.disabledBackgroundColor {
                self.backgroundColor = c
            }
            
            self.alpha = isEnabled ? 1 : 0.5
        }
    }
}

@IBDesignable
class RoundedUIButtonWithActivityIndicator: RoundedUIButton {
    
    @IBInspectable
    var indicatorColor : UIColor = .lightGray
    
    private var activityIndicator: UIActivityIndicatorView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.setTitle(self.title(for: .normal), for: .reserved)
        self.activityIndicator = createActivityIndicator()
    }
    
    func showSpinner() {
        self.isUserInteractionEnabled = false
        self.setTitle(self.title(for: .normal), for: .reserved)
        self.setTitle("", for: .normal)
        self.activityIndicator.startAnimating()
    }
    
    func hideSpinner() {
        self.activityIndicator.stopAnimating()
        self.setTitle(self.title(for: .reserved), for: .normal)
        self.isUserInteractionEnabled = true
    }
    
    private func createActivityIndicator() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = indicatorColor
        self.addSubview(activityIndicator)
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        let xCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: activityIndicator, attribute: .centerX, multiplier: 1, constant: 0)
        self.addConstraint(xCenterConstraint)
        
        let yCenterConstraint = NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: activityIndicator, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraint(yCenterConstraint)
        
        return activityIndicator
    }
    
    
}
