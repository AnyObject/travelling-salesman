//
//  NumberView.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 5/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

import UIKit

@IBDesignable
class NumberView: UIView {
    
    @IBInspectable
    public var text: String = "0" {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    
    @IBInspectable
    public var textColor: UIColor = .white {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    public var rounded: Bool = true
    
    @IBInspectable
    public var borderColor: UIColor = .white
    
    @IBInspectable
    public var borderWidth: CGFloat = 0
    
    var label: UILabel!
    
    override func draw(_ rect: CGRect) {
        // Drawing code
        
        if rounded {
            self.layer.cornerRadius = rect.height / 2
            self.clipsToBounds = true
        }
        
        //border
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        
        if label == nil {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: rect.width , height: rect.height))
            self.addSubview(label)
        }
        
        label.textAlignment = .center
        label.baselineAdjustment = .alignCenters
        label.textColor = textColor
        label.text = text
    }

    
    override var backgroundColor: UIColor? {
        didSet {
            self.setNeedsDisplay()
        }
    }

}
