//
//  AppDelegate.swift
//  Travelling Salesman
//
//  Created by Antonio Yip on 3/08/18.
//  Copyright © 2018 AnyObject Limited. All rights reserved.
//

/*App Icon made by Freepik from www.flaticon.com*/

import UIKit
import CoreLocation
import UserNotifications

var app: AppDelegate!

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let locationManager: CLLocationManager = CLLocationManager()
    
    var lastLocation: CLLocation? {
        didSet {
            NotificationCenter.default.post(name: Notification.Name("didSetLastLocation"), object: lastLocation)
        }
    }
        
    var destinations: [Destination] = {
        guard let array = UserDefaults.standard.object(forKey: "destinations") as? [[String: Any]]
            else { return [] }
        debugPrint("destinations: ", array)
        return array.flatMap({Destination(dictionary: $0)})
    }()
    {
        didSet {
            logDebug("destinations didSet: %@", destinations)
            UserDefaults.standard.set(self.destinations.map({$0.dictionary}), forKey: "destinations")
            NotificationCenter.default.post(name: .didSetDestinations, object: self)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        app = self
        
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if #available(iOS 11.0, *) {
            locationManager.showsBackgroundLocationIndicator = true
        }
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        UserDefaults.standard.synchronize()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.synchronize()
    }
}

extension AppDelegate {
    func requestNotificationAuthorization() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            
            center.delegate = self
            
            center.requestAuthorization(options: [.alert, .badge, .sound]) { (success, error) in
                if let e = error {
                    logError("requestAuthorization: %@", e.localizedDescription)
                }
                
                if success {
                    logInfo("requestAuthorization successed")
                }
            }
        } else {
            // Fallback on earlier versions
            DispatchQueue.main.async {
                let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
            
        }
    }
    
    
    func scheduleNotification(_ identifier: String, message: String, at date: Date, sound: String = UILocalNotificationDefaultSoundName) {
        
        requestNotificationAuthorization()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
            
            let content = UNMutableNotificationContent()
            
            content.body = message
            content.sound = .default()
            
            var trigger: UNCalendarNotificationTrigger?
            
            let calendar = Calendar(identifier: .gregorian)
            let components = calendar.dateComponents(in: .current, from: date)
            let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
            
            trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
            
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
            
            UNUserNotificationCenter.current().add(request) { (error) in
                if let e = error {
                    logError("UNNotificationRequest error: %@", e.localizedDescription)
                }
            }
        }
        else {
            DispatchQueue.main.async {
                UIApplication.shared.cancelAllLocalNotifications()
                
                let notification = UILocalNotification()
                notification.alertBody = message
                notification.soundName = UILocalNotificationDefaultSoundName
                
                notification.fireDate = date
                notification.timeZone = NSTimeZone.default
                notification.repeatInterval = NSCalendar.Unit(rawValue: 0)
            
                UIApplication.shared.presentLocalNotificationNow(notification)
            }
        }
    }
}

@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
    
   
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        logInfo("willPresent: %@", notification.description)
        
        NotificationCenter.default.post(name: .willPresentNotification, object: notification.request.content.body)
        
    }
}
